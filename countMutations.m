%%

expected = "GTTGACTTGCCTGCATCATGTGTGACTGAGTATTGGTGTAAAATCACCCGCCAGCAGATTATACCTGCTGGTTTTTTTTATTCTCGCCGCGCTAAAAAGGGAACGT";

mutationCounts = zeros(10,31);

for fileID = [11:30]
	filename = sprintf('./CSVs/anustup%d_R1.csv', fileID)
	mutationHist = zeros(10,1);

	file = fopen(filename);
	fgetl(file);
	while ~feof(file)
		line = fgetl(file);
		commaidx = find(line==',');
		observed = line(1:commaidx(1)-1);
		count = str2num(line(commaidx(1)+1:commaidx(2)-1));
		
		if length(observed) ~= length(expected)
			continue;
		end
		discrepancy = sum(observed ~= expected);
		discrepancy = min(9,discrepancy);
		mutationHist(discrepancy+1) += count;
	end
	fclose(file);
	
	mutationCounts(:,fileID) = mutationHist;
	continue;

	set(0, 'defaultaxesfontsize', 20);
	bar(0:1:9, mutationHist/1000, 'k');
	xlabel('# mutations');
	ylabel('# reads (1000)');

	print(sprintf('hist%d.png', fileID));
end

save '-mat-binary' 'counts.mat' 'mutationCounts';



