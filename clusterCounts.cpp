// Analysis code for sort-seq.

#include <vector>
#include <set>
#include "os.h"

using namespace std;


// Imports the raw sequencing output file during initialization. Note, if the input fastq file is too big for the available RAM, may cause an out of memory error.
vector<string> importFastq (const string filename) {
	fstream inputFile;
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false)
		throw("Error: File " + filename + " could not be read.\n");
	
	// Pre-allocate some space to improve performance.
	int estimatedNumReads = getFilesize(filename.c_str()) / (2*100); // 100 bp read is assumed, underestimation is better than overestimation for performance, but in no case it is fatal.
	vector <string> reads;
	reads.reserve(estimatedNumReads);
	
	// Import reads 4-line blocks at a time
	while (true) {
		// Import reads 4-line blocks at a time
		string trash;
		getline(inputFile, trash); // Discard info row starting with @
		if (inputFile.eof()==true)
			break;
		
		string sequence, qscore;
		getline(inputFile, sequence);
		getline(inputFile, trash); // discard '+' row
		getline(inputFile, qscore);
		
		if (qscore.size() != sequence.size()) {
			cout << "ERROR: Invalid FASTQ file format." << endl;
			throw(1);
		}
		
		// Record the read in the list. Only the relevant position excluding the amplification adaptors is stored ( positions [22,127] ) 
		reads.push_back( sequence.substr(22,106) );
	}
	inputFile.close();
	
	// Release excess memory that was previously allocated.
	reads.reserve(reads.size());
	return reads;
}

template <typename T1, typename T2>
struct operatorLess {
	bool operator() (const pair <T1,T2>& x, const pair <T1,T2>& y) const {
		return x.first < y.first;
	}
};


template <typename T1, typename T2>
struct operatorGreater {
	bool operator() (const pair <T1,T2>& x, const pair <T1,T2>& y) const {
		if (x.first > y.first)
			return true;
		else if (x.first < y.first)
			return false;
		else
			return x.second > y.second;	
	}
};
	
// Finds number of times each read is encountered during sequencing.
set < pair<string,int>, operatorLess<string,int> > countElements (const vector <string>& list) {	// First organise the data in a new structure.
	int numData = list.size();
	
	// Detect exact duplicates.
	set < pair<string,int>, operatorLess<string,int> > dataTree;
	for (int i=0; i<numData; i++) {
		pair <string,int> temp (list.at(i), 1); // first: DNA sequence, second: keeps track of corresponding element count.
		pair < set<pair<string,int> >::iterator, bool> ret = dataTree.insert(temp);
			  
		if (ret.second == false) {
			// This sequence already exists in the tree, increment the count of the existing entry.
			int hitCount = ret.first->second;
			temp.second = hitCount + 1;
			dataTree.erase(ret.first);
			dataTree.insert(temp);
		}
		// Else a new entry has been introduced to the tree.
	}
	
	return dataTree;
}


void outputCounts (const set < pair<string,int>, operatorLess<string,int> >& inputTree, const string outfilename) {
	// Build one more tree, so that the output will be sorted with respect to the number of reads.
	set < pair<int,string>, operatorGreater<int,string> > sortTree;
	for (set < pair<string,int> > :: iterator it = inputTree.begin(); it!=inputTree.end(); it++ ) {
		pair<int,string> temp (it->second, it->first);
		sortTree.insert(temp);
	}
	
	ofstream outfile;
	outfile.open( outfilename.c_str() );
	outfile << "DNA_SEQUENCE, number_of_reads" << endl;
	
	int totalCount = 0;
	for (set < pair<int,string> >::iterator it = sortTree.begin(); it!=sortTree.end(); it++ ) {
		int count = it->first;
		if (count >= 10)
			outfile << it->second << ", " << count << "," << endl;
		
		// If counts become too low, stop reporting but continue counting
		totalCount += count;
	}
	
	outfile << "TOTAL_NUM_READS, " << totalCount << "," << endl;
	outfile.close();
	return;
}


void processFile (const string filename) {
	// Import the data
	vector<string> fastqreads = importFastq (filename);
	cout << fastqreads.size() << endl;
	
	// Count distinct reads
	set < pair<string,int>, operatorLess<string,int> > tree = countElements (fastqreads);
	
	// Report the outcomes
	int parpos = filename.find_last_of(".")+1;
	string outfilename = filename.substr(0,parpos) + "csv";
	outputCounts (tree, outfilename);
	
	return;
}


// g++ fixseq.cpp -fopenmp && ./a.out <folder name>
int main (int argc, const char** argv) {
	if (argc < 2) {
		cout << "No directory to process provided." << endl;
		return -1;
	}
	string dirname(argv[1]);
	cout << "Folder to process: " << dirname << endl;
	
	try {
		vector <string> dirlist = listDirectory(dirname);
		dirlist = filterDirectoryList(dirlist, ".fastq");
		for (int i=0; i<dirlist.size(); i++) {
			string filename = dirlist.at(i);
			cout << "Processing " << filename << endl;
		
			double startTime = getCurrentTimeStamp();
			// -------------------------------------------------
			
			processFile(filename);
			
			// -------------------------------------------------
			double endTime = getCurrentTimeStamp();
			printElapsedTime (startTime, endTime, filename);
		}
		cout << "All tasks completed." << endl;	
	}
	
	catch (const char* err) {
		cout << err << endl;
	}
	
	return 0;
}

